package com.bojangrubisa.hrPlatform.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.bojangrubisa.hrPlatform.dtos.CandidateDTO;
import com.bojangrubisa.hrPlatform.exceptions.CandidateNotFoundException;
import com.bojangrubisa.hrPlatform.model.Candidate;
import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.repository.CandidateRepository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

@ExtendWith(MockitoExtension.class)
public class CandidateServiceTest {

    @Mock
    private CandidateRepository candidateRepository;
    @InjectMocks
    private CandidateService candidateService;

    @Mock
    private ModelMapper modelMapper;

    @Test
    public void shouldFindAll() {
        List<Candidate> candidates = new ArrayList<>();
        candidates.add(new Candidate(1L, "Test", LocalDate.now(), "1234567", "", new ArrayList<>()));
        when(candidateRepository.findAll()).thenReturn(candidates);
        Iterable<Candidate> actualValue = candidateService.findAll();

        Assertions.assertEquals(candidates, actualValue);
    }

    @Test
    public void shouldFindById() throws CandidateNotFoundException {
        Candidate candidate = new Candidate();
        Long id = 1L;
        candidate.setId(id);

        when(candidateRepository.getById(id)).thenReturn(candidate);
        Candidate actualValue = candidateService.findById(id);

        Assertions.assertEquals(candidate.getId(), actualValue.getId());
    }

    @Test
    public void shouldFindByName() throws CandidateNotFoundException {
        List<Candidate> candidates = new ArrayList<>();
        Candidate candidate = new Candidate();
        String name = "Test";
        candidate.setName(name);
        candidates.add(candidate);

        when(candidateRepository.getByName(name)).thenReturn(candidates);
        List<Candidate> actualValue = candidateService.findByName(name);

        Assertions.assertEquals(candidates, actualValue);
    }

    @Test
    public void shouldDelete() throws CandidateNotFoundException {
        Candidate candidate = new Candidate();
        candidate.setId(1L);
        Long id = 1L;

        candidateService.delete(id);
        when(candidateRepository.getById(id)).thenReturn(null);

        assertThrows(CandidateNotFoundException.class, () -> {
            candidateService.findById(id);
        });

    }

    @Test
    public void shouldSave() throws CandidateNotFoundException {

        Candidate candidate = new Candidate();
        candidate.setId(1L);
        candidate.setName("Test");
        candidate.setEmail("test@test.com");
        candidate.setContactNumber("12345678");
        candidate.setDateOfBirth(LocalDate.of(1980, 1, 1));
        candidate.setSkills(new ArrayList<>());

        when(candidateRepository.save(candidate)).thenReturn(candidate);
        Candidate actualValue = candidateService.save(candidate);

        when(candidateRepository.getById(candidate.getId())).thenReturn(candidate);
        actualValue = candidateService.findById(actualValue.getId());

        Assertions.assertEquals(candidate, actualValue);

    }

    @Test
    public void shouldEdit() throws CandidateNotFoundException {

        Long id = 1L;
        Candidate candidate = new Candidate();
        candidate.setId(id);
        candidate.setName("Test");
        candidate.setEmail("test@test.com");
        candidate.setContactNumber("12345678");
        candidate.setDateOfBirth(LocalDate.of(1980, 1, 1));
        candidate.setSkills(new ArrayList<>());

        when(candidateRepository.save(candidate)).thenReturn(candidate);

        candidate = candidateService.save(candidate);

        Candidate newCandidate = new Candidate();
        newCandidate.setId(id);
        newCandidate.setName("newTest");
        newCandidate.setEmail("newtest@test.com");
        newCandidate.setContactNumber("12121212");
        newCandidate.setDateOfBirth(LocalDate.of(1990, 1, 1));
        newCandidate.setSkills(new ArrayList<>());

        CandidateDTO candidateDTO = new ModelMapper().map(newCandidate, CandidateDTO.class);

        when(candidateRepository.save(newCandidate)).thenReturn(newCandidate);
        when(candidateRepository.getById(newCandidate.getId())).thenReturn(candidate);

        when(modelMapper.map(candidateDTO, Candidate.class)).thenReturn(newCandidate);
        candidate = candidateService.edit(id, candidateDTO);
        when(candidateRepository.getById(newCandidate.getId())).thenReturn(newCandidate);

        Candidate actualValue = candidateService.findById(id);

        Assertions.assertEquals(newCandidate, actualValue);
    }

    @Test
    public void shouldDeleteSkillFromCandidate() throws CandidateNotFoundException {
        Candidate candidate = new Candidate();
        Long id = 1L;
        Skill skill = new Skill();
        candidate.setId(id);
        candidate.setSkills(new ArrayList<Skill>());
        candidate.getSkills().add(skill);

        when(candidateRepository.getById(candidate.getId())).thenReturn(candidate);
        when(candidateRepository.save(candidate)).thenReturn(candidate);

        Candidate actualValue = candidateService.deleteSkillFromCandidate(id, skill);

        Assertions.assertEquals(0, actualValue.getSkills().size());
        Assertions.assertEquals(candidate, actualValue);
    }
}
