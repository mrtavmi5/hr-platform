package com.bojangrubisa.hrPlatform.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import com.bojangrubisa.hrPlatform.model.Candidate;
import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.service.CandidateService;
import com.bojangrubisa.hrPlatform.service.SkillService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TestData {

    private final Long DATA_NUMBER = 10L;

    @Autowired
    CandidateService candidateService;

    @Autowired
    SkillService skillService;

    @PostConstruct
    public void addData() {
        List<Candidate> candidates = new ArrayList<>();
        List<Skill> skills = new ArrayList<>();
        LocalDate date = LocalDate.of(1980, 1, 1);

        for (Long i = 1L; i <= DATA_NUMBER; i++) {
            Candidate candidate = new Candidate(i, "Test name" + i, date, "123456" + i, "email" + i + "@test.com",
                    new ArrayList<>());

            candidates.add(candidate);
            candidateService.save(candidate);

            Skill skill = new Skill(0L, "Skill " + i, new ArrayList<>());
            skills.add(skill);
            skill = skillService.save(skill);

            candidate.addSkill(skill);
            candidateService.save(candidate);

        }

    }

}
