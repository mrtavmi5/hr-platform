package com.bojangrubisa.hrPlatform.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class CandidateNotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    public CandidateNotFoundException(Long id) {
        super("Candidate with this id: " + id + " doesn't exists!");
    }

    public CandidateNotFoundException(String name) {
        super("Candidate with name: " + name + " doesn't exists!");
    }

}
