package com.bojangrubisa.hrPlatform.repository;

import com.bojangrubisa.hrPlatform.model.Skill;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

    Skill getById(Long id);

    Skill getByName(String name);
}
