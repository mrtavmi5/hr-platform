package com.bojangrubisa.hrPlatform.model;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Candidate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDate dateOfBirth;

    @Column(nullable = false)
    private String contactNumber;

    @Email(message = "Email is mandatory")
    @Column(nullable = false)
    private String email;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "candidate_skills", joinColumns = { @JoinColumn(name = "candidate_id") }, inverseJoinColumns = {
            @JoinColumn(name = "skill_id") })
    private List<Skill> skills;

    public void addSkill(Skill skill) {
        skills.add(skill);
        skill.getCandidates().add(this);
    }

    public void removeSkill(Skill skill) {
        skills.remove(skill);
        skill.getCandidates().remove(this);
    }
}
