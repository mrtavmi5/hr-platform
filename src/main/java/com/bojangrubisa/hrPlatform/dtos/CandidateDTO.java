package com.bojangrubisa.hrPlatform.dtos;

import java.time.LocalDate;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bojangrubisa.hrPlatform.model.Skill;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CandidateDTO {

    private Long id;

    @NotNull
    @NotBlank(message = "Name is mandatory")
    private String name;

    @NotNull
    @NotBlank(message = "Date is mandatory")
    private LocalDate dateOfBirth;

    @NotNull
    @NotBlank(message = "Contact number name is mandatory")
    private String contactNumber;

    @Email(message = "Email is mandatory")
    private String email;

    private List<Skill> skills;
}
