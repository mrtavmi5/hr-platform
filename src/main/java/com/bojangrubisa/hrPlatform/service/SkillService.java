package com.bojangrubisa.hrPlatform.service;

import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.repository.SkillRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SkillService {

    @Autowired
    private SkillRepository skillRepository;

    public Skill save(Skill skill) {
        return skillRepository.save(skill);
    }

}
