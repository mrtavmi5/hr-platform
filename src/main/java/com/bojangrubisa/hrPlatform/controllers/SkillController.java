package com.bojangrubisa.hrPlatform.controllers;

import com.bojangrubisa.hrPlatform.dtos.SkillDTO;
import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.service.SkillService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/skill")
@CrossOrigin
public class SkillController {

    @Autowired
    private SkillService skillService;
    @Autowired
    private ModelMapper modelMapper;

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public SkillDTO addSkill(@RequestBody SkillDTO skillDTO) {
        Skill skill = modelMapper.map(skillDTO, Skill.class);
        skillService.save(skill);

        return modelMapper.map(skill, SkillDTO.class);
    }

}
