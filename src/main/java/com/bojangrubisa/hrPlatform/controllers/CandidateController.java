package com.bojangrubisa.hrPlatform.controllers;

import java.util.List;

import com.bojangrubisa.hrPlatform.dtos.CandidateDTO;
import com.bojangrubisa.hrPlatform.dtos.SkillDTO;
import com.bojangrubisa.hrPlatform.exceptions.CandidateNotFoundException;
import com.bojangrubisa.hrPlatform.model.Candidate;
import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.service.CandidateService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/candidate")
@CrossOrigin
public class CandidateController {

    @Autowired
    private CandidateService candidateService;
    @Autowired
    private ModelMapper modelMapper;

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping
    public Iterable<Candidate> getAll() {
        return candidateService.findAll();
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/{id}")
    public Candidate getOne(Long id) throws CandidateNotFoundException {
        return candidateService.findById(id);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/search/{name}")
    public List<Candidate> getByName(String name) throws CandidateNotFoundException {
        return candidateService.findByName(name);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @GetMapping("/skill/{name}")
    public List<Candidate> searchAllCandidatesWithSkills(String name) {
        return candidateService.searchAllCandidatesWithSkills(name);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/{id}")
    public void delete(Long id) {
        candidateService.delete(id);
    }

    @ResponseStatus(code = HttpStatus.CREATED)
    @PostMapping
    public CandidateDTO addCandidate(@RequestBody CandidateDTO candidateDTO) {
        Candidate candidate = modelMapper.map(candidateDTO, Candidate.class);
        candidateService.save(candidate);

        return modelMapper.map(candidate, CandidateDTO.class);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @PutMapping("/{id}")
    public Candidate update(@RequestBody CandidateDTO editCandidateDTO, @PathVariable Long id)
            throws CandidateNotFoundException {

        return candidateService.edit(id, editCandidateDTO);
    }

    @ResponseStatus(code = HttpStatus.OK)
    @DeleteMapping("/skill/{id}")
    public Candidate deleteSkillFromCandidate(@RequestBody SkillDTO skillDTO, @PathVariable Long id)
            throws CandidateNotFoundException {

        Skill skill = modelMapper.map(skillDTO, Skill.class);

        return candidateService.deleteSkillFromCandidate(id, skill);
    }

}
