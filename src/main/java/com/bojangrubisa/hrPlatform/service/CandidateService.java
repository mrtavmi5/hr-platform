package com.bojangrubisa.hrPlatform.service;

import java.util.List;

import com.bojangrubisa.hrPlatform.dtos.CandidateDTO;
import com.bojangrubisa.hrPlatform.exceptions.CandidateNotFoundException;
import com.bojangrubisa.hrPlatform.model.Candidate;
import com.bojangrubisa.hrPlatform.model.Skill;
import com.bojangrubisa.hrPlatform.repository.CandidateRepository;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Iterable<Candidate> findAll() {
        return candidateRepository.findAll();
    }

    public Candidate findById(Long id) throws CandidateNotFoundException {
        Candidate candidate = candidateRepository.getById(id);
        if (candidate == null) {
            throw new CandidateNotFoundException(id);
        }
        return candidate;
    }

    public List<Candidate> findByName(String name) throws CandidateNotFoundException {
        List<Candidate> candidate = candidateRepository.getByName(name);
        if (candidate == null) {
            throw new CandidateNotFoundException(name);
        }
        return candidate;
    }

    public void delete(Long id) {
        candidateRepository.deleteById(id);
    }

    public Candidate save(Candidate candidate) {
        return candidateRepository.save(candidate);
    }

    public Candidate edit(Long id, CandidateDTO editCandidateDTO) throws CandidateNotFoundException {

        findById(id);
        Candidate candidate = modelMapper.map(editCandidateDTO, Candidate.class);
        candidate.setId(id);
        save(candidate);

        return candidate;

    }

    public Candidate deleteSkillFromCandidate(Long id, Skill skill) throws CandidateNotFoundException {
        Candidate candidate = findById(id);

        List<Skill> skills = candidate.getSkills();
        skills.remove(skill);
        save(candidate);

        return candidate;
    }

    public List<Candidate> searchAllCandidatesWithSkills(String skillName) {
        return candidateRepository.findBySkillsNameLike(skillName);

    }
}
