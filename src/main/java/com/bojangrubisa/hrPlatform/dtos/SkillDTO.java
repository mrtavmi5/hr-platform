package com.bojangrubisa.hrPlatform.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillDTO {

    private Long id;

    @NotNull
    @NotBlank(message = "Skill name is mandatory")
    private String skillName;
}
