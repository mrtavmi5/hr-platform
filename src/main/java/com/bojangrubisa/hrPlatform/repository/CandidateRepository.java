package com.bojangrubisa.hrPlatform.repository;

import java.util.List;

import com.bojangrubisa.hrPlatform.model.Candidate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long> {

    Candidate getById(Long id);

    List<Candidate> getByName(String name);

    List<Candidate> findBySkillsNameLike(String skillName);

}
